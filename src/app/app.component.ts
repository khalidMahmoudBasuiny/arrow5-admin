import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{

  title = 'ARROW Admin';

  defaultLanguage: string | any = "ar";

  defaultLanguageForm: FormGroup | any;


  languages = [
    {id: 1, name: "ar", value: "ar"},
    {id: 2, name: "en", value: "en"}
  ];

  constructor(private translate: TranslateService, private formBuilder: FormBuilder)
  {
  }

  ngOnInit()
  {
    this.defaultLanguageForm = this.formBuilder.group({
      defaultLanguageCTRL: [null]
    });
  }

  changeDefaultLanguage(value:any)
  {
    this.defaultLanguage = value;
    localStorage.setItem("arrow_default_language", this.defaultLanguage);

    this.translate.setDefaultLang(this.defaultLanguage);
    this.translate.use(this.defaultLanguage);
  }

}
