import {CmsGroup} from "./CmsGroup";
import {CmsContact} from "./CmsContact";
import {Substitution} from "./Substitution";
import {CmsAccessProfile} from "./CmsAccessProfile";
import {CmsUserSettings} from "./CmsUserSettings";
import {CmsTaskUser} from "./CmsTaskUser";
import {ChartNode} from "./ChartNode";
import {UserRoleSignature} from "./UserRoleSignature";
import {TreeHtml} from "./TreeHtml";
import {UserStamp} from "./UserStamp";

export class CmsUser
{

  userId: number;
  contentUserId: string;
  userName: string;
  displayName: string;
  sessionId: string;
  email: string;
  active: boolean;
  isMember: boolean;
  onBehalf: boolean;
  substituted: boolean;
  mobileToken: string;
  password: string;
  encryptedPassword: string;
  domain: string;
  domainPrefix: string;
  repository: string;
  ip: string;
  host: string;
  dateCreated: Date;
  lastUpdated: Date;
  contactName: string;
  updatedDate: Date;
  activationValue: string;
  statusValue: string;

  userGroups: CmsGroup[];
  userContactInfo: CmsContact;
  substitions: Substitution[];
  subtituterInfo: Substitution;
  userProfile: CmsAccessProfile;
  settings: CmsUserSettings;
  substitutedTaskUsers: CmsTaskUser[];
  parentNodes: ChartNode[];
  userSignatures: UserRoleSignature[];
  treeHtml: TreeHtml;
  userStamp: UserStamp;

}
