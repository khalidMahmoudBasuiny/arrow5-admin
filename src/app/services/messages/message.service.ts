import {Injectable} from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class MessageService
{

  constructor(private toastrService: ToastrService)
  {
  }

  addSuccess(message: string)
  {
    this.toastrService.success(message);
  }

  addInfo(message: string)
  {
    this.toastrService.info(message);
  }

  addWarning(message: string)
  {
    this.toastrService.warning(message);
  }

  addError(message: string)
  {
    this.toastrService.error(message);
  }

}
