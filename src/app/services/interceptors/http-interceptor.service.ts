import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor
{

  constructor()
  {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
  {
    if (req.url.match('/oauth/token') && req.method === 'POST')
    {
      const changedReq = req.clone({
        headers: req.headers
          .set('Access-Control-Allow-Origin', '*')
      });
      return next.handle(changedReq);

    } else if ((req.url.match('/assets/i18n') && req.method === 'GET'))
    {
      // if translation request
      const changedReq = req.clone({});
      return next.handle(changedReq);
    } else
    {
      // Other requests

      // let user: CmsUser = this.authenticationService.currentUserValue;

      // if (user != null && user.token != null)
      // {
      const changedReq = req.clone({
        url: `${environment.apiUrl}${environment.context}` + '/' + req.url,
        headers: req.headers
          .set('Access-Control-Allow-Origin', '*')
          .set('Set-Cookie', 'SameSite=None')
        // .set('authorization', 'Bearer ' + user.token)
      });
      return next.handle(changedReq);
      // } else
      // {
      //   return throwError('unauthorized user');
      // }

    }
  }
}
