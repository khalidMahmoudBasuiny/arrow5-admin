import {NgxUiLoaderService} from "ngx-ui-loader";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class UILoader
{
  constructor(private ngxService: NgxUiLoaderService)
  {
  }

  startLoader()
  {
    this.ngxService.start();
  }

  closeLoader()
  {
    this.ngxService.stop();
  }

}
