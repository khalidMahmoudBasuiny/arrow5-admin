import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {DOCUMENT} from "@angular/common";
import {UILoader} from "../../services/UILoader/UILoader";
import {MessageService} from "../../services/messages/message.service";
import {CssLoaderService} from "../../utils/css/css-loader.service";

interface Language
{
  name: string,
  code: string
}

export const ImageURL = {
  LOGO: '../../../assets/Images/arrow-logo-wt.png',
  CUSTOMER: '../../../assets/Images/Logos/mcs-logo.png'
};


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit
{

  logoURL: string = ImageURL.LOGO;

  customerLogoURL: string = ImageURL.CUSTOMER;

  authorized: boolean = true;

  loginForm: FormGroup;

  languages: Language[] | any;

  checked: boolean = false;

  fieldTextType: boolean;


  constructor(private formBuilder: FormBuilder,
              private translate: TranslateService,
              @Inject(DOCUMENT) private document: Document,
              private uiLoader: UILoader,
              private messageService: MessageService,
              private cssLoaderService: CssLoaderService)
  {
    this.cssLoaderService.load("login.component.ar.css");
    this.initLoginForm();
    this.initLanguages();
  }

  ngOnInit(): void
  {
    this.uiLoader.startLoader();
    this.uiLoader.closeLoader();
  }

  initLoginForm()
  {
    this.loginForm = this.formBuilder.group({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required]),
      language: new FormControl("ar", [Validators.required]),
      domain: new FormControl("", [Validators.required]),
      forceLogin: new FormControl("", [Validators.required]),
      rememberMe: new FormControl(null)
    });
  }

  initLanguages()
  {
    this.languages = [
      {name: 'العربية', value: 'ar'},
      {name: 'English', value: 'en'}
    ];
  }

  changeDefaultLanguage()
  {
    let language = this.loginForm.controls['language'].value;
    localStorage.setItem("arrow_default_language", language);

    this.translate.setDefaultLang(language);
    this.translate.use(language);
    this.cssLoaderService.load("login.component." + language + ".css");
  }

  toggleFieldTextType()
  {
    this.fieldTextType = !this.fieldTextType;
  }

}
