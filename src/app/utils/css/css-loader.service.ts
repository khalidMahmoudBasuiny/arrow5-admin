import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class CssLoaderService {

  constructor(@Inject(DOCUMENT) private document: Document) { }

  load(fileName: string)
  {
    const head = this.document.getElementsByTagName('head')[0];

    let themeLink = this.document.getElementById(
      'client-theme'
    ) as HTMLLinkElement;

    if (themeLink)
    {
      themeLink.href = fileName;
    } else
    {
      const style = this.document.createElement('link');
      style.id = 'client-theme';
      style.rel = 'stylesheet';
      style.href = `${fileName}`;

      head.appendChild(style);
    }
  }
}
